--- 
layout: post
permalink: nunettokencontracts.html
title: nunettokencontracts
nav_order: 1
has_children: true
date:  2022-03-17
---



<h2 align=center> nunettokencontracts </h2>


    # nunet-token-contracts
Includes token contracts, migrations, tests

### NuNetToken
* ERC-20 implementation for NuNet NTX Token

## Deployed Contracts
* NuNetToken (Mainnet): 
* NuNetToken (Kovan): 
* NuNetToken (Ropsten) : 

## Requirements
* [Node.js](https://github.com/nodejs/node) (12+)
* [Npm](https://www.npmjs.com/package/npm)

## Install

### Dependencies
```bash
npm install
```

### Test 
```bash
npm run test
```

## Package
```bash
npm run package-npm
```

## Release
NuNetToken artifacts are published to NPM: https://www.npmjs.com/package/nunet-token-contracts
=======
ERC-20 implementation for NuNet NTX Token
    
    


<hr>


Hash: 4845e278653dbb37b8e2c75b596e6f197d1215fa


GitHub: [nunettokencontracts](https://gitlab.com/nunet/nunet-token-contracts/-/blob/main/README.md)


	
