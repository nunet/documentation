--- 
layout: post
permalink: Ai Dsl Test.html
title: Ai Dsl Test
nav_order: 1
has_children: true
date:  2022-03-17
---



<h2 align=center> Ai Dsl Test </h2>


    # Ai Dsl Test

## Clone repos

``` 
    bash clone.sh all
```

## Build docker images
``` 
    bash build.sh all

```

## Run services
``` 
    export db_name=
    export user_name=
    export password=
    
    bash up.sh all

```
(see actually exported values here: https://gitlab.com/nunet/integration-tests/-/issues/25)

## make call to adapter1


``` 
    docker exec -it nunet-adapter-news-score-ai-dsl bash -c "python3 test_ai_dsl.py"

```
    
    


<hr>


Hash: df9fe9838d4faf383be8c6f3a357aba77b902bf4


GitHub: [Ai Dsl Test](https://gitlab.com/nunet/nunet-infra/-/blob/master/ai-dsl/README.md)


	
