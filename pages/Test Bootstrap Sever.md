--- 
layout: post
permalink: Test Bootstrap Sever.html
title: Test Bootstrap Sever
nav_order: 1
has_children: true
date:  2022-03-17
---



<h2 align=center> Test Bootstrap Sever </h2>


    Test Bootstrap Sever

## Step 1

---

Steps to be performed on testnet

- Clone the nunet-infra repo
- `cd nunet-infra/ipv8-boot-strap-server/job-definition`
- `nomad job run bootstrap_server.hcl`
- from nomad.icog-labs.com get the allocation address i.e ip:port of bootstrap-server job

## Step 2

---

Setup to run an IPv8 node on the raspberry pis

- Clone the protocol-engineering repo inside misc-experiments on each device
- `cd protocol-engineering/ipv8-node/
- Setup requirements as per `README.md`
- Add the bootstrap-server address from Step1 into the Dispersary_Bootstraper dictionary inside `configuration.py`
- Make sure that all the devices are in the same community. This can be set by a `community_id` inside `main.py`
- `python main.py` runs the node and continuously prints the known peers.
    
    


<hr>


Hash: e20300958c1136718b39b811321efa633578ecdf


GitHub: [Test Bootstrap Sever](https://gitlab.com/nunet/nunet-infra/-/blob/master/ipv8-boostrap-server/README.md)


	
