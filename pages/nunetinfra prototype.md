--- 
layout: post
permalink: nunetinfra prototype.html
title: nunetinfra prototype
nav_order: 1
has_children: true
date:  2022-03-17
---



<h2 align=center> nunetinfra prototype </h2>


    # nunet-infra prototype

This is based on nomad, consul, fabio + docker. It assumes docker system is pre-installed correctly and installs . The rest should work as follows:

## step 1

The nomad installation is controlled via `adapter.sh` script, which:

* `sh adapter.sh install` downloads and installs newest nomad, installs configuration on the system, etc. requires `sudo`;
* `sh adapter.sh start server` and `sh adapter.sh start client` starts server and client separately in production...
  * there should be at least one server for nomad/consul cluster but they cannot run on one machine; client scripts should indicate the address and port of servers so that they could communicate. current client scripts have hardcoded values corresponding to *.icog-labs.com nomad and consul servers; in the future, when the nomad-infra grows, server addresses will be taken as parameters to the script or dynamically resolved;
* `sh adapter.sh start dev` sarts nomad agent in dev mode (i.e. single server and single client);
**Use only 'dev' mode or 'server & client' mode, but not together -- they will screw up each other and system reboot may be required...**
* `sh adapter.sh status` shows the status of nomad agent;
* `sh adapter.sh stop dev` -- stops nomad agent;

## step 2

There are two ways to deploy a test network made of *fake_news_score* and *uclnlp* docker modules:

* `nomad job run job-definitions/fake_news_detection.hcl` allocates the job into nomad manually;
* `python test.py` does exactly the same but from python interface. This will be integrated into containers that will do orchastration;

## step 3

Check how things work by looking to `http://localhost:4646/ui/jobs`
    
    


<hr>


Hash: 2058aee8ea37ee363167b871310fcf2e4214a508


GitHub: [nunetinfra prototype](https://gitlab.com/nunet/nunet-infra/-/blob/master/README.md)


	
