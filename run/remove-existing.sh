git clone https://doc_pages:${doc_pages}@gitlab.com/nunet/documentation.git

cd ./documentation/pages/
find . \! -name 'test' -delete

git config  user.name "nunet-io"
git config  user.email "accounts@nunet.io"

git add .
git commit -m 'removed exising'
git pull origin main
git push --push-option='ci.skip' origin main
