mkdir repos
cd ./repos

wget https://gitlab.com/nunet/documentation/-/raw/main/run/repo_list.txt

filename='repo_list.txt'
while read repos; do 
    git clone --depth 1 $repos 
done < "$filename"

ls

find . -name '*.md' > fileslist.dat

mkdir ../readme_folder


while read file
do
	DIR=`echo $(dirname $file)`
	tempstr=`echo ${file:2}`
	stripath=`echo "${tempstr#*/}"`


	pushd $DIR
	remote=`echo $(git ls-remote --get-url)`
	branch="$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')"
	popd


	remote+="/-/blob/$branch/"
	remote+=$stripath

	hash=`echo $(git hash-object $file)`
    filename=`echo ${file%.md}`
    line=`echo $(head -n 1 $file)`
    cleanline=`echo ${line//[^[:alnum:] ]/}`
    date=`echo $(date +%F)`

    echo "$(echo -n '--- 
layout: post
permalink: '$cleanline'.html
title: '$cleanline'
nav_order: 1
has_children: true
date:  '$date'
---



<h2 align='center'> '$cleanline' </h2>


    '; cat $filename.md)
    
    " > $filename.md

echo "

<hr>


Hash: $hash


GitHub: [$cleanline]($remote)


	" >> $filename.md
    
    mv $filename.md ../readme_folder/"$cleanline".md

done <  fileslist.dat


cd ..
git clone https://doc_pages:${doc_pages}@gitlab.com/nunet/documentation.git
rm -r ./documentation/pages/.
cp -u -r readme_folder/* ./documentation/pages/.
ls
cd ./documentation/

git config  user.name "nunet-io"
git config  user.email "accounts@nunet.io"

git add .
git commit -m 'documentation files.md'
git push --push-option='ci.skip' origin main

cd ..
rm -r *
