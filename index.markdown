---
layout: home
---

<table style="width:100%">
	<tr>
		<td style="width:40%;" align="center">
			<img style="display:block;" width="20%" src="{{site.baseurl}}/assets/images/nunet_logo_light.png" />
		</td>
	</tr>
</table>

# NuNet

NuNet is a platform for decentralized compute resource sharing where users with latent compute resources can contribute to the platform and enable it to be used by the network. The NuNet platform only provides the framework with which compute resources can be shared and utilized. Users have the freedom to choose what to use it for. The generic user groups that can make use of the platform are:
- Compute Providers: These are users who have one or more desktop, laptop, singleboard computers, or smartphones or any other compute device lying around and would like to contribute it to the network. These users onboard their devices on the NuNet platform. When doing so, they choose how much resource they would like to be used by the network. Compute Providers earn tokens based on the amount of resources have been used by the network.
- Service Developers: These are users who have built some online service with a clearly defined API and would like to deploy their services on the NuNet platform. These services can be anything as long as they can be packaged in a container and have a specific format for data input/output. These users will have their services hosted on the devices the Compute Provider user group has contributed to the network and hence will pay for the amount of resources they use.
- Service Consumers: These are users who need to use services deployed by the Service Developers. The Service Consumers pay for the services they are using to the Service Developers. 

These groups are the most general categorization of the NuNet Platform users. On specific cases, there are more user groups depending on the use case the platform offers. More on this in X.X 

Because NuNet enables optimal arrangement of data, compute resources and process, while also allowing machine-to-machine payment, it can be utilized for anything from IoT applications where decentralized edge computing is desired to AI/ML services for facial recognition and text summarization. 

Currently NuNet allows onboarding a device on the network, installing the NuNet adapter and setting the amount of resources to be used by the network for a fake news warning app to be deployed on the device. This fake news warning app is a simple demo constituting a browser extension which serves to demonstrate “service user”, a backend application - fake news score which is an ensemble program which gets results from two different scorers - a stance detection program and a binary classifier. Depending on the amount of resources the device provider has set, one of these backend programs will be installed to it. 

This demo app was built to demonstrate the NuNet platform for the private alpha. More on the private alpha on Roadmap - Private Alpha.


## Components

1. Onboarding script : The onboarding script is a way to onboard a device to the NuNet network. 
2. NuNet Adapter : The NuNet Adapter functions sort of like a sidecar proxy where it handles communication with the network and also service request-response proxy. The NuNet adapter is the component that maintains the network connection, maintains the DHT registry, the deployment of service on the device and the decoupler of the service request-response between the p2p overlay network and the services running on the device.
3. Global Orchestrator : The Global Orchestrator is a centralized registry for all devices on the NuNet network and the services running on them. It is currently being depreciated in favor of the DHT (Distributed Hash Tables) solution where the registry for peers and services is kept in a decentralized manner instead of on the centralized Global Registry. This Registry functions by keeping track of the services running on devices and the endpoints they expect to receive input through. A NuNet adapter can send a request querying the registry to get information about other peers and their services.


<script src="https://use.fontawesome.com/6328c1a6f3.js"></script>
