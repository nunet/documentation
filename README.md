## documentation

This is an old repo which is currently only used for the Wiki pages that contain currently best available documentation of the internal team processes. These pages are already old and need to be updated. Additionally we will move them into another repository. After that, this repository will be archived.